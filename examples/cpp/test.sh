#!/bin/bash
SERVER=${ALOHA_SERVER:-https://rtaloha-collector.parity.digital/}
make clean all
./build/example --server_url=$SERVER --event=Event1 --upload=true --debug=true
./build/example --server_url=$SERVER --values=Value1 --upload=true --debug=true
./build/example --server_url=$SERVER --event=Event2 --values=Value2 --upload=true --debug=true
./build/example --server_url=$SERVER --event=Event3 --values=Foo=foo,Bar=bar --upload=true --debug=true
